python>=3.7
numpy
numba
h5py
pytest
snakemake-minimal
sra-tools