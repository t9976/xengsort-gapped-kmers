"""
Definition of a Choice Hash Table with the 'Power of Two Choices' (tm)
"""

from math import ceil
from collections import namedtuple

import numpy as np
from numba import njit, int64

from .dnaencode import generate_revcomp_and_canonical_code
from .generalhash import generate_get_page_and_get_fingerprint, print_info

ChoiceHash = namedtuple("ChoiceHash",
    ["hashtype",
     "hashtable",
     "get_page1",
     "get_page2",
     "get_fingerprint1",
     "get_fingerprint2",
     "store_code",
     "get_genome_for_code",
     "get_occupancy",
     "memorybits",
    ])


def generate_choice_hash(q, pagebits, fprbits, pagesize, hf1, hf2):
    """
    Allocate an array and compile access methods for a hash table.
    
    Parameters:
    q:        q-gram length; all fingerprints are based on q-grams (2q bits)
    pagebits: number of bits for indexing a hash table page;  pagebits < 2*q
    fprbits:  number of bits for fingerprints;  pagebits + fprbits <= 2*q
    pagesize: number of slots in one hash page; the total hash table size is
              2**pagebits * pagesize * ceil((fprbits+3) / 8) bytes
    hf1:      name of hash function (first choice)
    hf2:      name of hash function (second choice)

    Return a ChoiceHash namedtuple instance.
    """
    qbits = 2*q
    if fprbits < 0:
        fprbits = qbits - pagebits
    if fprbits + pagebits > 2*q or pagebits <= 0:
        raise ValueError("must have 0 < pagebits <= pagebits+fprbits <= 2*q")
    if fprbits > 60:
        raise ValueError("must have fprbits <= 60")
    slotbits = fprbits + 3
    assert slotbits <= 63
    pfprbits = fprbits + 1
    pagesizebits = slotbits * pagesize  # e.g. 64 for fprbits=14, pagesize=4  

    tablebits = int(2**pagebits * pagesizebits)
    tablewords = int(ceil(tablebits / 64))
    hashtable = np.zeros(tablewords, dtype=np.int64)
    print_info("ChoiceHash", slotbits, pagesize, pagesizebits, pagebits, tablewords)
    
    # structure of an item in a slot:  ---ggpffffffff
    # gg: two genome bits
    # p: one page choice bit
    # fffffff: fingerprint bits
    fprmask = int(2**fprbits - 1)
    
    # define get_page{1,2}(code) and get_fingerprint{1,2}(code) functions
    (get_page1, get_fingerprint1) = generate_get_page_and_get_fingerprint(
            hf1, q, pagebits, fprbits)
    (get_page2, get_fingerprint2) = generate_get_page_and_get_fingerprint(
            hf2, q, pagebits, fprbits)

    # Extract the item on a given page in a given slot
    @njit(locals=dict(x=int64, mask1=int64, a=int64, b=int64, b1=int64))
    def get_item(table, page, slot):
        """
        Return the full item x from the given (table, page, slot).
        The bits x=(gg|p|f...f) must be separated to get genome, pagechoice, fingerprint.
        """
        # must have 0 <= page < 2**pagebits and 0 <= slot < pagesize
        startbit = page * pagesizebits + slot * slotbits
        a = startbit // 64  # item starts in table[a]
        b = startbit & 63   # at bit number b
        if b + slotbits <= 64:  # get slotbits bits
            # bits are contained in a single int64
            x = table[a] >> b
        else:
            # bits are distributed over two int64s, 
            # leftmost b1 = 64-b in table[a], rightmost b2 = slotbits-b1 in table[a+1]
            b1 = 64 - b
            mask1 = (1<<b1) - 1
            x = (table[a] >> b) & mask1 
            x |= (table[a+1] << b1)
        return x

    @njit(locals=dict(page=int64, slot=int64, f=int64, p=int64, g=int64,
              v=int64, v1=int64, startbit=int64, a=int64, 
              b=int64, b1=int64, b2=int64, mask1=int64, mask2=int64))
    def set_item(table, page, slot, f, p, g):
        """
        store the item x from fingerprint f, pagechoice p, genome g
        at the given table, page, slot combination,
        overwriting what was there previously without checking.
        """
        # set fingerprint/pagechoice/genome bits to f/p/g, respectively
        v = (f & fprmask) | ((p&1) << fprbits) | ((g&3) << pfprbits)  # slotbits
        startbit = page * pagesizebits + slot * slotbits
        a = startbit // 64
        b = startbit & 63
        if b + slotbits <= 64:
            # bits are contained in a single int64
            mask1 = ~(((1<<slotbits) - 1) << b)
            table[a] = (table[a] & mask1) | (v << b)
        else:
            # b1 leftmost bits in table[a] = b1 rightmost bits of v, 
            b1 = 64 - b  # b1 leftmost bits in table[a]
            mask1 = (1<<b) - 1  # only keep b rightmost bits
            v1 = (v & ((1<<b1) - 1))
            table[a] = (table[a] & mask1) | (v1 << b)
            # b2 rightmost bits in table[a+1] = b2 leftmost bits of v
            b2 = slotbits - b1
            mask2 = ~((1<<b2) - 1)
            table[a+1] = (table[a+1] & mask2) | (v >> b1)

    @njit(locals=dict(fpr1=int64, fpr2=int64, page1=int64, page2=int64,
            slot1=int64, slot2=int64, x=int64, g=int64, p=int64, f=int64))
    def _get_pagestatus(table, fpr1, fpr2, page1, page2):
        # return (0, slot1, genome1) if code found on page choice 1
        # return (1, genome2, slot2) if code found on page choice 2
        # return (-1, fill1, fill2) if code not found on either page
        for slot1 in range(pagesize):
            x = get_item(table, page1, slot1)
            g = (x >> pfprbits) & 3
            if g == 0:
                break  # not found on page1
            p = (x >> fprbits) & 1
            if p != 0:
                continue  # item from different pagechoice
            f = x & fprmask
            if f == fpr1:
                return (0, slot1, g)
        else:  # nobreak, slots exhausted
            slot1 = pagesize
        # code not found on page1, slot1 contains full slots of page1
        for slot2 in range(pagesize):
            x = get_item(table, page2, slot2)
            g = (x >> pfprbits) & 3
            if g == 0:
                break  # not found on page2
            p = (x >> fprbits) & 1
            if p != 1:
                continue  # item from different pagechoice
            f = x & fprmask
            if f == fpr2:
                return (1, g, slot2)
        else:  # nobreak, slots exhausted
            slot2 = pagesize
        # code not found on page1/2; slot1/2 contains full slots of page1/2
        return (-1, slot1, slot2)
    
    @njit(locals=dict(code=int64, base=int64, slot=int64, x=int64, f=int64, g=int64))
    def store_code(code, genome, table):
        """
        Attempt to store given code with given genome in hash table.
        Return values:
        -2: Code and genome were already present
        -1: Code was present, new genome information was added
         0: New code was added with given genome information
         1: Overflow, page was full
        """
        page1 = get_page1(code)
        fpr1 = get_fingerprint1(code)
        page2 = get_page2(code)
        fpr2 = get_fingerprint2(code)
        (p, slot1, slot2) = _get_pagestatus(table, fpr1, fpr2, page1, page2)
        if p == -1:
            if slot1 <= slot2:
                # store on page choice 1, slot1
                if slot1 >= pagesize:
                    return 1  # overflow
                set_item(table, page1, slot1, fpr1, 0, genome)
            else:
                # store on page choice 2, slot2
                set_item(table, page2, slot2, fpr2, 1, genome)
            return 0
        if p == 0:
            # found on page choice 1, slot1
            g = slot2  # not zero!
            newgenome = (g|genome) != g
            if newgenome:
                set_item(table, page1, slot1, fpr1, 0, (g|genome))
                return -1
            return -2
        else:
            # p==1: found on page choice 2, slot2
            g = slot1  # not zero!
            newgenome = (g|genome) != g
            if newgenome:
                set_item(table, page2, slot2, fpr2, 1, (g|genome))
                return -1 
            return -2  

            
    @njit(locals=dict(code=int64, page1=int64, fpr1=int64, page2=int64, fpr2=int64, slot1=int64, slot2=int64, p=int64))
    def get_genome_for_code(code, table):
        """
        return genome information (0,1,2,3) for given code,
        or -1 if code was not found but both pages are full,
        return -2 in case of internal error.
        """
        page1 = get_page1(code)
        fpr1 = get_fingerprint1(code)
        page2 = get_page2(code)
        fpr2 = get_fingerprint2(code)
        (p, slot1, slot2) = _get_pagestatus(table, fpr1, fpr2, page1, page2)
        if p == -1:
            if slot1 < pagesize or slot2 < pagesize:
                return 0  # not found, but space available
            return -1  # not found, and both pages are full
        elif p == 0:
            return slot2  # for p=0, slot2 is the genome at slot1
        elif p == 1:
            return slot1  # for p=1, slot1 is the genome at slot2
        return -2  # error


    @njit(locals=dict(page=int64, last=int64, slot=int64, x=int64, g=int64))
    def get_occupancy(table):
        pagefill = np.zeros(pagesize+1, dtype=np.int64)
        genomefill = np.zeros(4, dtype=np.int64)
        for page in range(2**pagebits):
            last = -1
            for slot in range(pagesize):
                x = get_item(table, page, slot)
                g = (x >> pfprbits) & 3
                genomefill[g] += 1
                if g != 0:
                    last = slot
            pagefill[last+1] += 1
        return pagefill, genomefill


    return ChoiceHash(
        hashtype="choice",
        hashtable=hashtable,
        get_page1=get_page1,
        get_page2=get_page2,
        get_fingerprint1=get_fingerprint1,
        get_fingerprint2=get_fingerprint2,
        store_code=store_code,
        get_genome_for_code=get_genome_for_code,
        get_occupancy=get_occupancy,
        memorybits=tablebits,
        )
