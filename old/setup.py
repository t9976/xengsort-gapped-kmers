# coding: utf-8
NAME = "xengsort"

import sys
try:
    from setuptools import setup
except ImportError:
    print("Please install setuptools before installing {}.".format(NAME), file=sys.stderr)
    exit(1)

if sys.version_info < (3,5):
    print("At least Python 3.5 is required for {}.\n".format(NAME), file=sys.stderr)
    exit(1)


# load and set VERSION and DESCRIPTION
vcontent = open("{}/version.py".format(NAME)).read()
exec(vcontent) 

setup(
    name=NAME,
    version=VERSION,
    author='Sven Rahmann',
    author_email='sven.rahmann@uni-due.de',
    description=DESCRIPTION,
    zip_safe=False,
    license='MIT',
    url='None',
    packages=[NAME],
    entry_points={
        "console_scripts": [
            "{} = {}.main:main".format(NAME, NAME),
        ],
    },
    package_data={'': ['*.css', '*.sh', '*.html']},
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ]
)
